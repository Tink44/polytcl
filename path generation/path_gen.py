import numpy as np
import tkinter as tk
import time,os
from tkinter import filedialog
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import shortest_path

def import_csv(path = ""):

    if path == "":

        root = tk.Tk()
        root.withdraw()

        path = filedialog.askopenfilename()

    matrix = np.genfromtxt(path, delimiter = ',')

    return matrix

def best_path_gen(graph):

    graph  = csr_matrix(graph)

    dist , pred = shortest_path(csgraph=graph, directed=False, return_predecessors=True)

    return dist , pred

def gen_path_matrix(pred):

    n = np.size(pred[0])

    path_matrix = [[None]*n for _ in range(n)]

    for i in range(n):
        for j in range(n):
            if pred[i][j] >= 0 :
                path = [j]
                j_ = j
                while j != i :
                    j = pred[i][j]
                    path.append(j)
                path_matrix[i][j_] = path[::-1]

    return path_matrix

def export_csv(matrix , path = "", delimiter = ',', cell_del = ":"):

    if path == "":

        root = tk.Tk()
        root.withdraw()

        path = filedialog.asksaveasfilename()

    n = len(matrix)

    csv_matrix = ""

    for i in range(n):

        line = ""

        for j in range(n):

            cell = ""

            if (matrix[i][j] != None) :

                for k in range(len(matrix[i][j])):

                    cell = cell + str(matrix[i][j][k]) + cell_del

                cell = cell[:len(cell)-1]
            
            else : cell = "None"

            line = line + cell + delimiter
        
        line = line[:len(line)-1] + "\n"
        
        csv_matrix = csv_matrix + line

    with open(path,'w',newline='') as csvfile : csvfile.write(csv_matrix)

    return

def main(out_msg = False, out_dtl = True):

    root = tk.Tk()
    root.withdraw()

    imp_path = filedialog.askopenfilename(initialdir = os.path.abspath('path generation//files//in'))
    exp_path = filedialog.asksaveasfilename(title = 'Save Best path CSV as',defaultextension = '.csv',filetypes=(('CSV','*.csv'),),initialdir = os.path.abspath('path generation//files//out'))

    start = time.time()
    in_matrix = import_csv(path = imp_path)
    time1 = time.time()
    dist_matrix, pred_matrix = best_path_gen(in_matrix)
    time2 = time.time()
    path_matrix = gen_path_matrix(pred_matrix)
    time3 = time.time()
    export_csv(path_matrix, path=exp_path)
    end = time.time()

    imp_time = time1-start
    dist_pred_time = time2-time1
    path_time = time3-time2
    exp_time = end-time3

    if out_dtl == True:

        print("#==============#===========#=======#")
        print("| Value        | Data      | Unit  |")
        print("#==============#===========#=======#")
        print("| Export       | OK        |   -   |")
        print("#--------------#-----------#-------#")
        print(f"| Stops        | {len(path_matrix):9d} |   -   |")
        print("#--------------#-----------#-------#")
        print(f"| Import       | {(imp_time*10e3):9.3f} |  ms   |")
        print(f"| Predecessors | {(dist_pred_time*10e3):9.3f} |  ms   |")
        print(f"| Paths        | {(path_time*10e3):9.3f} |  ms   |")
        print(f"| Export       | {(exp_time*10e3):9.3f} |  ms   |")
        print("#--------------#-----------#-------#")
        print(f"| Total        | {((end-start)*10e3):9.3f} |  ms   |")
        print("#==============#===========#=======#")

    if (out_msg == True) & (out_dtl == False):

        print("Best paths found, export succesfull")

main(out_dtl=True)