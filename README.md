# PolyTCL

## Description

Cet outil a pour but de calculer la matrice des trajets optimaux sur un reseau ferroviaire entre chaques gares.  
Le calcul se fait en fonction des temps de trajets connus entre chacunes des gares.

The purpose of this tool is to calculate the matrix of optimal routes on a rail network between each station.  
The calculation is based on known travel times between each station.


## Utilisation / How to

Le calcul se fait à l'aide de la fonction `main()` du script `path_gen.py`.  
Il prend en entrée une matrice des temps entre chaques gares sous forme de fichier CSV.  
Il export après calcul une matrice de tous les trajets optimaux dans un second CSV.

The calculation is performed using the `main()` function in the `path_gen.py` script.  
It takes as input a matrix of times between each station in the form of a CSV file.  
After calculation, it exports a matrix of all optimal routes as another CSV file.


## Fichier CSV / CSV file

Le fichier d'entrée doit etre un CSV de forme carré (dimension NxN), avec des virgules (,) comme séparateurs.  
Il doit contenir exclusivement les temps de trajet, pas d'intitulé, de titre de colonne ou de ligne.  
Les temps de trajet doivent utiliser une unité de temps commune, les décimales doivent etre indiquées par un point (.) pour ne pas interferer avec la virgule (,) qui est utilisée pour le fichier CSV.  
Les temps de trajets nuls (0) sont utilisés quand les gares n'ont pas de ligne direct les connectant et sur la diagonale principale de la matrice des temps.  
Un exemple est disponible par la suite.

The input file must be a square CSV (dimension NxN), with commas (,) as separators.  
It must contain travel times only, no headings, column or row titles.  
Journey times must use a common time unit, decimals must be indicated by a dot (.) to avoid interfering with the comma (,) used for the CSV file.  
Zero travel times (0) are used when stations have no direct line connecting them and on the main diagonal of the time matrix.  
An example is provided below.

### Exemple de CSV / CSV template

Graph d'exemple de réseau / Example network graph :

```mermaid
graph TD;

    A-.25s.-B;
    A-.12,50s.-C;
    A-.25s.-D;
    C-.12s.-E;
    D-.656ms.-E;
    F-.7min35s.-G;
```

Table des temps de parcours correspondant / Table of corresponding journey times:

| tps (s) | **A**  | **B**  | **C**  | **D**  | **E**  | **F**  | **G**  |
|---------|--------|--------|--------|--------|--------|--------|--------|
| **A**   | _null_ | 25     | 12.5   | 25     |        |        |        |
| **B**   |        | _null_ |        |        |        |        |        |
| **C**   |        |        | _null_ |        | 12     |        |        |
| **D**   |        |        |        | _null_ | 0.656  |        |        |
| **E**   |        |        |        |        | _null_ |        |        |
| **F**   |        |        |        |        |        | _null_ | 455    |
| **G**   |        |        |        |        |        |        | _null_ |

Contenu du CSV correspondant / Corresponding CSV content:

```
0,25,12.5,25,0,0,0
0,0,0,0,0,0,0
0,0,0,0,12,0,0
0,0,0,0,0.656,0,0
0,0,0,0,0,0,0
0,0,0,0,0,0,455
0,0,0,0,0,0,0
```

## Librairies

En plus des bibliothèques incluses `tkinter`, `time` et `os`, ce script utilise aussi [***Numpy***](https://numpy.org/) et [***Scipy***](https://scipy.org/).

In addition to the included `tkinter`, `time` and `os` libraries, this script also uses [***Numpy***](https://numpy.org/) and [***Scipy***](https://scipy.org/).